# Introduction
The VISTA group in the [USC Information Sciences Institute](https://isi.edu/) focuses on the cutting edge research problems and real-life applications in video, image, speech and also multimedia analysis. Our past projects include but not limited to 
* multi-language handwritten script recognition
* face recognition in the wild
* video event analysis
* event prediction over social networks
* multimedia forensics analysis

Due to the big data nature of our problems, we are always looking for scalable and trainable solutions. As a result, we are very interested in student workers with good programming skills and strong backgrounds in mathematics/statistics, physics, machine learning, and deep learning. 

# Task Description
To select best candidates, we design a multi-task learning problem for applicants: 

    Given an input image, you are expected to 
        Task 1: find all text regions, if any
        Task 2: identify the dominant script language from {Arabic, Chinese, English, Hebrew, Russian, Unknown}

Below we visualize several sample input images (left) and output results (right), where 
* text regions are highlighted in terms of green rectangles, 
* the name of the dominant script languge is highlighed in red at the upper-left corner.

![](https://gitlab.com/rex-yue-wu/ISI-VISTA-Challenge/raw/master/docs/vista_sample_1.png)
![](https://gitlab.com/rex-yue-wu/ISI-VISTA-Challenge/raw/master/docs/vista_sample_2.png)
![](https://gitlab.com/rex-yue-wu/ISI-VISTA-Challenge/raw/master/docs/vista_sample_6.png)
![](https://gitlab.com/rex-yue-wu/ISI-VISTA-Challenge/raw/master/docs/vista_sample_3.png)
![](https://gitlab.com/rex-yue-wu/ISI-VISTA-Challenge/raw/master/docs/vista_sample_4.png)
![](https://gitlab.com/rex-yue-wu/ISI-VISTA-Challenge/raw/master/docs/vista_sample_5.png)


# Data Description
In this repository, you will find a big zip file named `data.zip`. After you download it or clone this project, you should first unzip this file to get all training and testing samples. 

If you use osx or linux, you can simply unzip this file in your terminal via the command
```bash
    unzip -P $PASSWORD data.zip
```
Please simply replace `$PASSWORD` with the actual password that you received in our advertisement. 
Alternatively, you may login your USC email and send me a message to ask the password. 

Assume you successfully unzip `data.zip`, you are expected to see 
- **Train** (dir for training dataset)
- **Test** (dir for testing dataset)
- train.list (txt file for all training images)
- test.list (txt file for all testing images)

**Train** folder contains 20,000 images with labels, and **Test** folder contains 2,000 images without labels. All images are of the JPEG format, while all labels are of the JSON format. 

Specifically, given a training image file `${basename}.jpg`, you can find its text bounding boxes and dominant script language in its corresponding label file `${basename}.json`, which contains two name/value pairs, namely,

    text_box_list: 
        a list of text bounding boxes, where each box is a list of [boxTopLeft_x, boxTopLeft_y, boxWidth, boxHeight] in terms of pixels.
        
    language: 
        a value in { ARABIC, CHINESE, ENGLISH, RUSSIAN, HEBREW, UNKNOWN }, where UNKNOWN is for non-text images.

For example, the label file of the last sample ("Millennials: All Digital") looks like below

    {'text_bbox_list': [[153, 40, 429, 50], [72, 129, 475, 38], [73, 327, 585, 29], [72, 366, 256, 24], [73, 205, 610, 38], [77, 252, 182, 24]], 'language': 'ENGLISH'}

# Submission Instructions
For each test image, you should perform both text detection and language identification tasks, and save your results in a JSON file following the naming convention and the label format as you see for training data.

Once you finish predictions for all (2,000) test images, you should zip your results (only those JSON files) into a single file `YOUR_NAME_USCID.zip` and email it to me. 

We also encourage you to put your source code into a git repo and send us its link in email, but this is NOT mandontary. 

# Notes
In short, you can do whatever you believe reasonable to solve the problem
1. There are absolutely NO restrictions on programming language(s), machine learning package(s), or deep learning toolbox(es). 
2. There is NO restriction on your solution or method.  
3. There is NO restriction on data preprocessing/postprocessing. 

The only restriction we have is that 

    you should train your model ONLY with the provided dataset

implying that you should **NOT** rely on any external dataset. 

Please expect that a small amount of samples are not correctly annotated, e.g. some text region is not listed. 

Finally, the most important thing:

***We are NOT looking for a perfect solution, but a talented student with great potential like you. So please do not be afraid to submit your solution.***


# Contact
If you have any question regarding to this challenge, please contact

    Dr. Yue Wu 
    yue_wu@isi.edu
I will get back to you as soon as possible. 








